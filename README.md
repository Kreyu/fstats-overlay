# fstats-overlay

> Fortnite Battle Royale stats overlay made for streamers.
> Created in Vue, using [Fortnite Tracker API](https://fortnitetracker.com/site-api).

**LIVE DEMO**  
http://swroblewski.pl/fstats/Kreyuu

## Build Setup

``` bash
# Install dependencies
npm install
```

At this point, you **have** to:
- Rename `config/dev.env.js.example` to `config/dev.env.js`
- Rename `config/prod.env.js.example` to `config/prod.env.js`
- Provide your own API key from Fortnite Tracker in both files

``` bash
# Serve with hot reload at localhost:8080
npm run dev

# Build for production with minification
npm run build

# Build for production and view the bundle analyzer report
npm run build --report
```

## Usage

Simply open URL providing username, and optionally speed in ms.  
For example: `localhost:8080/Ninja?speed=2500`
