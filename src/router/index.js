import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '../components/Homepage'
import Statistics from '../components/Statistics'

Vue.use(Router)

export default new Router({
  base: '/fstats',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    },
    {
      path: '/:username',
      name: 'Statistics',
      component: Statistics
    }
  ]
})
