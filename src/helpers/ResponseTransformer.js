export default class ResponseTransformer {
  static prepareForTranslation (response) {
    if (response.isValid()) {
      return response.data.lifeTimeStats.map(a => a.key)
    }

    return [ response.error ]
  }

  static mergeTranslations (response, collection) {
    if (response.isValid()) {
      let data = response.data.lifeTimeStats

      data.forEach((el, index) => {
        el.key = collection[index]
      })

      response.data.lifeTimeStats = data

      return response
    }

    // Error messages are always first in collection
    response.error = collection[0]

    return response
  }
}
