const Messages = {
  PL: require('./../assets/translations.pl.json'),
  EN: require('./../assets/translations.en.json')
}

export default class Translator {
  constructor (locale = 'EN', fallback = 'EN') {
    this._locale = locale.toUpperCase()
    this._fallback = fallback.toUpperCase()

    return this
  }

  get locale () {
    return this._locale
  }

  get fallback () {
    return this._fallback
  }

  set locale (locale) {
    this._locale = locale.toUpperCase()
  }

  set fallback (fallback) {
    this._fallback = fallback.toUpperCase()
  }

  translate (key) {
    const localeMessages = Messages[this._locale]
    const fallbackMessages = Messages[this._fallback]

    if (key in localeMessages) {
      return localeMessages[key]
    }

    if (key in fallbackMessages) {
      return fallbackMessages[key]
    }
  }

  translateMany (collection) {
    collection.forEach((record, index) => {
      collection[index] = this.translate(record)
    })

    return collection
  }
}
