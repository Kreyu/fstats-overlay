import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.CORS_PROXY + 'api.fortnitetracker.com/',
  timeout: 5000,
  headers: {
    'TRN-Api-Key': process.env.API_KEY
  }
})

function checkResponse (response) {
  const isPlayerFound = typeof response.data.error === 'undefined'
  const isRedirection = 'lifetimeStats' in response.data
  const isTimeout = response.status === 408

  if (isRedirection || !isPlayerFound) {
    response.error = 'Player Not Found'
  }

  if (isTimeout) {
    response.error = 'Connection Timeout'
  }

  response.valid = !isRedirection && !isTimeout && isPlayerFound

  response.isValid = function () {
    return this.valid
  }

  return response
}

export default {
  async get (url) {
    let response = {}

    try {
      response = await instance.get(url)
    } catch (e) {
      response = e.code !== 'ECONNABORTED'
        ? e.response : {
          status: 408,
          data: {}
        }
    }

    return checkResponse(response)
  }
}
